nom
===

A name generator.

Usage
-----

Usage: parahash [OPTION]
       Defaults to using 32 bytes of entropy.
  -rep string
       hash representation - hex, base58, bip39 or proquint (default "hex")
  -seed string
        read the bytes to hash as the parameter of this argument
  -stdin
        read the bytes to hash from stdin rather than as an argument
